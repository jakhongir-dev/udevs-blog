import "./App.scss";
import UdevsLogo from "./components/UdevsLogo/UdevsLogo";
import Login from "./components/Login/Login";
import Navbar from "./components/Navbar/Navbar";
import Blogs from "./components/Blogs/Blogs";
import Footer from "./components/Footer/Footer";
import BlogsContainer from "./components/Blogs/BlogsContainer";

function App() {
  return (
    <div className="app">
      <div className="header">
        <UdevsLogo />
        <Login />
      </div>
      <Navbar />
      <BlogsContainer>
        <Blogs />
      </BlogsContainer>
      <Footer />
    </div>
  );
}

export default App;
