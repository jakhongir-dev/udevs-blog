import { initializeApp } from "firebase/app";

import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBaZvXCw2mX2w1dF9JeRu97ZJ7eVe431eg",

  authDomain: "udevs-blog1.firebaseapp.com",

  databaseURL: "https://udevs-blog1-default-rtdb.firebaseio.com",

  projectId: "udevs-blog1",

  storageBucket: "udevs-blog1.appspot.com",

  messagingSenderId: "518986633905",

  appId: "1:518986633905:web:c5808538c896dfeb514890",

  measurementId: "${config.measurementId}",
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
export const auth = getAuth(app);
