import img from "../../../../../img/blog.jpg";
import style from "../../../ProfilePosts/ProfilePost/ProfilePost.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";

export default function Post({ title, description }) {
  return (
    <div className={style.profile_post}>
      <div className={style.post}>
        <img src={img} />
        <div className={style.con}>
          <h1>{title}</h1>
          <div className={style.stats}>
            <p>22:22</p>
            <p>02:12:2021</p>
            <p>|</p>
            <p>
              <FontAwesomeIcon className="blog-eye" icon={faEye} />
              789
            </p>
            <a href="#">Права человека</a>
          </div>
          <p>{description}</p>
          <button>Читать</button>
        </div>
      </div>
    </div>
  );
}
