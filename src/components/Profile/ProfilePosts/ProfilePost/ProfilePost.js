import React, { useCallback, useEffect, useState } from "react";
import { collection, getDocs } from "firebase/firestore";
import { db } from "../../../../firebase";
import Post from "./Post";

export default function ProfilePost() {
  const [items, setItems] = useState([]);

  const getItems = useCallback(async () => {
    const querySnapshot = await getDocs(collection(db, "profile-posts"));
    const fetchedData = await querySnapshot.forEach((doc) => {
      setItems((prev) => [...prev, doc.data()]);
    });
  }, []);

  useEffect(() => {
    getItems();
    return () => {
      setItems([]);
    };
  }, []);

  return items ? (
    items.map((item, index) => (
      <Post key={index} title={item.title} description={item.description} />
    ))
  ) : (
    <div>no data</div>
  );
}
