import React from "react";
import Login from "../Login/Login";
import UdevsLogo from "../UdevsLogo/UdevsLogo";
import Navbar from "../Navbar/Navbar";
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import ProfilePosts from "./ProfilePosts/ProfilePosts";

export default function Profile() {
  return (
    <div className="profile">
      <div className="header">
        <UdevsLogo />
        <Login />
      </div>
      <Navbar />
      <ProfileInfo />
      <h1 className="publication">Публикации</h1>
      <ProfilePosts />
    </div>
  );
}
