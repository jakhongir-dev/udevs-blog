import React from "react";
import ProfileImg from "../../../img/author.jpg";
import style from "./ProfileInfo.module.scss";

export default function ProfileInfo() {
  return (
    <div className={style.profile_info}>
      <img src={ProfileImg} />
      <div className={style.con}>
        <h1>Дилором Алиева</h1>
        <div className={style.conn}>
          <div className={style.con_1}>
            <p>Карьера</p>
            <p>Дата рождения</p>
            <p>Место рождения</p>
          </div>
          <div className={style.con_2}>
            <p>Писатель</p>
            <p>2 ноябряб 1974 ( 46 лет )</p>
            <p>Черняховскб СССР (Россия)</p>
          </div>
        </div>
      </div>
    </div>
  );
}
