import React, { useEffect, useState } from "react";
import "./Blogs.css";
import { useHistory } from "react-router-dom";
import DateViews from "./DateViews";
import { db } from "../../firebase";
import { collection, getDocs } from "@firebase/firestore";

export default function Blogs() {
  const [Posts, setPosts] = useState("");
  const postsCollectionRef = collection(db, "posts");

  let history = useHistory();
  function pageHandler(e) {
    const pageNum = e.target.closest(".blogs").id;
    history.push("blogs/" + pageNum);
  }
  useEffect(() => {
    const getPosts = async () => {
      const data = await getDocs(postsCollectionRef);
      setPosts(data.docs.map((doc) => doc.data()));
    };
    getPosts();
  }, []);
  return (
    Posts &&
    Posts.map((item) => {
      return (
        <div className="blogs" id={item.id} key={item.id} onClick={pageHandler}>
          <div className="img-container">
            <img className="blog__img" alt="Blogs" src={item.image}></img>
            <DateViews />
          </div>
          <p className="blogs__title">{item.title}</p>
        </div>
      );
    })
  );
}
