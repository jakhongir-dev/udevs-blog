import React from "react";
import "./DateViews.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye } from "@fortawesome/free-solid-svg-icons";

export default function DateViews() {
  return (
    <div className="date-n-views">
      <p className="time">22:22</p>
      <p className="date">02:12:2021</p>
      <p className="stick">|</p>
      <p className="views">
        <FontAwesomeIcon className="blog-eye" icon={faEye} />
        789
      </p>
    </div>
  );
}
