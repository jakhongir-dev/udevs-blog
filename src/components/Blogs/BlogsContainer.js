import React from "react";
import "./BlogsContainer.css";

export default function BlogsContainer(props) {
  const classes = "blogs-container " + props.className;
  return <div className={classes}>{props.children}</div>;
}
