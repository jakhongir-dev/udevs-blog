import React, { useRef, useCallback } from "react";
import style from "./CreatePost.module.scss";
import Login from "../Login/Login";
import UdevsLogo from "../UdevsLogo/UdevsLogo";
import Navbar from "../Navbar/Navbar";
import Footer from "../Footer/Footer";
import { db } from "../../firebase";
import { doc, setDoc } from "firebase/firestore";
import { useHistory } from "react-router";

export default function CreatePost() {
  const titleRef = useRef();
  const descriptionRef = useRef();
  const history = useHistory();

  const saveHandler = useCallback(() => {
    setDoc(doc(db, "profile-posts", new Date(Date.now()).toISOString()), {
      title: titleRef.current.value,
      description: descriptionRef.current.value,
    }).then((res) => {
      history.push("/profile");
    });
  }, []);

  return (
    <div className={style.create_post}>
      <div className="header">
        <UdevsLogo />
        <Login />
      </div>
      <Navbar />
      <h1>Настройки публикации</h1>
      <div className={style.form}>
        <label>Название</label>
        <input ref={titleRef} className={style.input_title} />
        <label>Описание</label>
        <textarea ref={descriptionRef} />
      </div>
      <button onClick={saveHandler}>Save</button>
      <Footer />
    </div>
  );
}
