import React from "react";
import "./Navbar.css";

export default function Navbar() {
  return (
    <nav className="nav">
      <ul className="nav__list">
        <li className="nav__item">
          <a href="#" className="nav__link nav__link--active">
            Все потоки
          </a>
        </li>
        <li className="nav__item">
          <a href="#" className="nav__link">
            Разработка
          </a>
        </li>
        <li className="nav__item">
          <a href="#" className="nav__link">
            Администрирование
          </a>
        </li>
        <li className="nav__item">
          <a href="#" className="nav__link">
            Дизайн
          </a>
        </li>
        <li className="nav__item">
          <a href="#" className="nav__link">
            Менеджмент
          </a>
        </li>
        <li className="nav__item">
          <a href="#" className="nav__link">
            Маркетинг
          </a>
        </li>
        <li className="nav__item">
          <a href="#" className="nav__link">
            Научпоп
          </a>
        </li>
      </ul>
    </nav>
  );
}
