import React from "react";
import "./UdevsLogo.css";
import { useHistory } from "react-router";

export default function UdevsLogo({ size }) {
  const history = useHistory();

  return (
    <div className={`logo logo__${size}`} onClick={() => history.push("/")}>
      <span className="logo__span">u</span>devs
    </div>
  );
}
