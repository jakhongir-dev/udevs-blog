import React from "react";
import UdevsLogo from "../UdevsLogo/UdevsLogo";
import "./Footer.css";
export default function Footer() {
  return (
    <div className="footer">
      <div className="footer__udevs">
        <UdevsLogo size="small" />
        <p className="footer__text">
          Помощник в публикации статей, журналов. Список популярных
          международных конференций. Всё для студентов и преподавателей.
        </p>
      </div>
      <ul className="footer__resources">
        <li className="resources__item">
          <a className="resources__link highlighted">Ресурсы</a>
        </li>
        <li className="resources__item">
          <a className="resources__link">Статьи</a>
        </li>
        <li className="resources__item">
          <a className="resources__link">Журналы</a>
        </li>
        <li className="resources__item">
          <a className="resources__link">Газеты</a>
        </li>
        <li className="resources__item">
          <a className="resources__link">Диплом</a>
        </li>
      </ul>
      <ul className="footer__about">
        <li className="about__item">
          <a className="about__link  highlighted">О нас</a>
        </li>
        <li className="about__item">
          <a className="about__link">Контакты</a>
        </li>
        <li className="about__item">
          <a className="about__link">Помощь</a>
        </li>
        <li className="about__item">
          <a className="about__link">Заявки</a>
        </li>
        <li className="about__item">
          <a className="about__link">Политика</a>
        </li>
      </ul>
      <ul className="footer__help">
        <li className="help__item">
          <a className="help__link  highlighted">Помощ</a>
        </li>
        <li className="help__item">
          <a className="help__link">Часто задаваемые вопросы</a>
        </li>
      </ul>
    </div>
  );
}
