import React from "react";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell, faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import "./Login.css";
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";
import { auth } from "../../firebase";
import profile from "../../img/author.jpg";
import { useHistory } from "react-router";

export default function Login() {
  const [IsOpen, setIsOpen] = useState(false);
  const [IsRegOpen, setRegIsOpen] = useState(false);
  const [IsLogged, setIsLogged] = useState(false);
  const [User, setUser] = useState({});

  const [registerEmail, setRegisterEmail] = useState("");
  const [registerPass, setRegisterPass] = useState("");
  const [loginEmail, setLoginEmail] = useState("");
  const [loginPass, setLoginPass] = useState("");
  const [isClicked, setisClicked] = useState(false);

  onAuthStateChanged(auth, (currentUser) => {
    setUser(currentUser);
  });

  async function registerHandler() {
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        registerEmail,
        registerPass
      );
      setRegIsOpen(false);
    } catch (error) {
      console.log(error.message);
    }
  }

  async function loginHandler() {
    try {
      const user = await signInWithEmailAndPassword(
        auth,
        loginEmail,
        loginPass
      );
      closeHandler();
      setIsLogged(true);
    } catch (error) {
      console.log(error.message);
    }
  }
  async function logoutHandler() {
    await signOut(auth);
  }
  function loginViaReg() {
    setRegIsOpen(false);
    loginModalHandler();
  }
  function loginModalHandler() {
    setIsOpen(true);
  }
  function regModalHandler() {
    setRegIsOpen(true);
  }

  function closeHandler() {
    setIsOpen(false);
  }
  function closeRegHandler() {
    setRegIsOpen(false);
  }
  function onSubmit(e) {
    e.preventDefault();
  }

  function profileHandler(e) {
    setisClicked((prev) => !prev);
  }

  let history = useHistory();

  return (
    <div className="login-container">
      <FontAwesomeIcon className="bell-icon" size={"3x"} icon={faBell} />
      {User && !IsLogged && (
        <button onClick={loginModalHandler} className="btn-login">
          Войти
        </button>
      )}
      {User && IsLogged && (
        <>
          <img onClick={profileHandler} className="profile-img" src={profile} />
          {isClicked && (
            <ul className="pro-list">
              <li className="pro-item">
                <a
                  onClick={() => history.push("/create-post")}
                  className="pro-link"
                >
                  Написать публикации
                </a>
              </li>
              <li className="pro-item">
                <a className="pro-link">Избранные</a>
              </li>
              <li className="pro-item">
                <a className="pro-link">Выйти</a>
              </li>
            </ul>
          )}
          <button onClick={logoutHandler} className="btn-logout">
            Выйти
          </button>
        </>
      )}
      {!User && (
        <button onClick={regModalHandler} className="btn-reg">
          Регистрация
        </button>
      )}
      <div className="modal">
        <h1 className="modal__title">Login</h1>
      </div>
      <div>
        <div className={`modal ${IsOpen ? "visible" : ""}`}>
          <h3 className="modal__title">Вход на udevs news</h3>
          <div className="form" onSubmit={onSubmit}>
            <input
              onChange={(e) => setLoginEmail(e.target.value)}
              className="input email"
              type="email"
              placeholder="Email"
            />
            <input
              onChange={(e) => {
                setLoginPass(e.target.value);
              }}
              className="input pass"
              type="password"
              placeholder="Пароль"
            />
            <button onClick={loginHandler} className="btn_login" type="submit">
              Войти
            </button>
          </div>

          <FontAwesomeIcon
            onClick={closeHandler}
            className="btn-close"
            size="4x"
            icon={faTimesCircle}
          />
        </div>
        {IsOpen && <div onClick={closeHandler} className="overlay" />}
      </div>
      <div>
        <div className={`modal ${IsRegOpen ? "visible" : ""}`}>
          <h3 className="modal__title">Регистрация на udevs news</h3>
          <div className="form">
            <input
              onChange={(e) => setRegisterEmail(e.target.value)}
              className="input email"
              type="text"
              placeholder="Email"
            />
            <input
              onChange={(e) => setRegisterPass(e.target.value)}
              className="input pass"
              type="password"
              placeholder="Пароль"
            />
            <button onClick={registerHandler} className="btn_login">
              Регистрация
            </button>
            <h1 onClick={loginViaReg} className="login_reg">
              Войти
            </h1>
          </div>

          <FontAwesomeIcon
            onClick={closeRegHandler}
            className="btn-close"
            size="4x"
            icon={faTimesCircle}
          />
        </div>
        {IsRegOpen && <div onClick={closeRegHandler} className="overlay" />}
      </div>
    </div>
  );
}
