import React from "react";
import blog from "../../img/blog.jpg";
import DateViews from "../Blogs/DateViews";
import "./Best.css";

export default function Best() {
  return (
    <div className="best-container">
      <h1 className="best-title">ЛУЧШИЕ БЛОГИ</h1>
      <div className="post_container">
        <img className="post-img2" src={blog} />
        <div className="dt-title">
          <p className="best__title">
            ВОЗ: молодые жители Европы стали меньше курить
          </p>
          <DateViews />
        </div>
      </div>
      <div className="post_container">
        <img className="post-img2" src={blog} />
        <div className="dt-title">
          <p className="best__title">
            ВОЗ: молодые жители Европы стали меньше курить
          </p>
          <DateViews />
        </div>
      </div>
      <div className="post_container">
        <img className="post-img2" src={blog} />
        <div className="dt-title">
          <p className="best__title">
            ВОЗ: молодые жители Европы стали меньше курить
          </p>
          <DateViews />
        </div>
      </div>
      <div className="post_container">
        <img className="post-img2" src={blog} />
        <div className="dt-title">
          <p className="best__title">
            ВОЗ: молодые жители Европы стали меньше курить
          </p>
          <DateViews />
        </div>
      </div>
    </div>
  );
}
