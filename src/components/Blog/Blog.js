import React from "react";
import "./Blog.css";
import UdevsLogo from "../UdevsLogo/UdevsLogo";
import Footer from "../Footer/Footer";
import Author from "./Author";
import Post from "./Post";
import Best from "./Best";

export default function Blog() {
  return (
    <div className="blog">
      <div className="blog__header">
        <UdevsLogo />
        <nav className="nav">
          <ul className="nav__list">
            <li className="nav__item">
              <a href="#" className="nav__link">
                Все потоки
              </a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">
                Разработка
              </a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">
                Администрирование
              </a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">
                Дизайн
              </a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">
                Менеджмент
              </a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">
                Маркетинг
              </a>
            </li>
            <li className="nav__item">
              <a href="#" className="nav__link">
                Научпоп
              </a>
            </li>
          </ul>
        </nav>
        <button className="blog-login">Войти</button>
      </div>
      <main className="main">
        <Author />
        <Post />
        <Best />
      </main>
      <Footer />
    </div>
  );
}
