import React, { useEffect, useState } from "react";
import "./Post.css";
import DateViews from "../Blogs/DateViews";
import { useHistory } from "react-router";
import { db } from "../../firebase";
import { collection, getDocs } from "@firebase/firestore";

export default function Post() {
  const [Posts, setPosts] = useState("");
  const postsCollectionRef = collection(db, "posts");

  let history = useHistory();
  let path = history.location.pathname;
  path = path.replace("/blogs/", "");

  useEffect(() => {
    const getPosts = async () => {
      const data = await getDocs(postsCollectionRef);
      setPosts(data.docs.map((doc) => doc.data()));
    };
    getPosts();
  }, []);
  return (
    Posts &&
    Posts.filter((item) => item.id == path).map((item) => {
      return (
        <div key={item.id} className="post-container">
          <img className="post-img" src={item.image} />
          <p className="post-author">
            Фото: <span>Dilorom Alieva</span>
          </p>
          <DateViews />
          <h1 className="blog-title">{item.title}</h1>
          <p className="blog-text">{item.description}</p>
        </div>
      );
    })
  );
}
