import React from "react";
import "./Author.css";
import author from "../../img/author.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark } from "@fortawesome/free-solid-svg-icons";
import Profile from "../Profile/Profile";
import { BrowserRouter as Router, Route, useHistory } from "react-router";

export default function Author() {
  let history = useHistory();
  function profileRouteHandler() {
    history.push("/profile");
  }
  return (
    <div className="author_container">
      <img onClick={profileRouteHandler} className="author__img" src={author} />
      <h1 className="author__name">Dilorom Alieva</h1>
      <div className="btn-container">
        <button className="btn-follow">Follow</button>
        <FontAwesomeIcon className="bookmark" size="5x" icon={faBookmark} />
      </div>
    </div>
  );
}
