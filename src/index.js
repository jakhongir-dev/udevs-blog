import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Blog from "./components/Blog/Blog";
import Profile from "./components/Profile/Profile";
import CreatePost from "./components/CreatePost/CreatePost";

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Switch>
        <Route exact path="/">
          <App />
        </Route>
        <Route exact path="/blogs/:id">
          <Blog />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
        <Route exact path="/create-post">
          <CreatePost />
        </Route>
      </Switch>
    </Router>
  </React.StrictMode>,
  document.getElementById("root")
);
